# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='NycInspections',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('camis', models.IntegerField(null=True, db_column=b'CAMIS', blank=True)),
                ('name', models.CharField(max_length=50, null=True, db_column=b'Name', blank=True)),
                ('borough', models.CharField(max_length=20, null=True, db_column=b'Borough', blank=True)),
                ('building', models.IntegerField(null=True, db_column=b'Building', blank=True)),
                ('street', models.CharField(max_length=50, null=True, db_column=b'Street', blank=True)),
                ('zipcode', models.CharField(max_length=10, null=True, db_column=b'Zipcode', blank=True)),
                ('phone', models.CharField(max_length=12, null=True, db_column=b'Phone', blank=True)),
                ('cuisine', models.CharField(max_length=30, null=True, db_column=b'Cuisine', blank=True)),
                ('inspection_date', models.DateField(null=True, db_column=b'Inspection_date', blank=True)),
                ('action', models.CharField(max_length=50, null=True, db_column=b'Action', blank=True)),
                ('violation_code', models.CharField(max_length=3, null=True, db_column=b'Violation_code', blank=True)),
                ('violation_description', models.CharField(max_length=240, null=True, db_column=b'Violation_description', blank=True)),
                ('critical_flag', models.CharField(max_length=30, null=True, db_column=b'Critical_flag', blank=True)),
                ('score', models.IntegerField(null=True, db_column=b'Score', blank=True)),
                ('grade', models.CharField(max_length=1, null=True, db_column=b'Grade', blank=True)),
                ('grade_date', models.DateField(null=True, db_column=b'Grade_date', blank=True)),
                ('record_date', models.DateField(null=True, db_column=b'Record_date', blank=True)),
                ('inspection_type', models.CharField(max_length=50, null=True, db_column=b'Inspection_type', blank=True)),
            ],
            options={
                'db_table': 'NYC_inspections',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Trip',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('medallion', models.CharField(max_length=50, blank=True)),
                ('hack_license', models.CharField(max_length=50, blank=True)),
                ('vendor_id', models.CharField(max_length=50, blank=True)),
                ('rate_code', models.IntegerField(null=True, blank=True)),
                ('store_fwd_flag', models.CharField(max_length=10, blank=True)),
                ('pickup_time', models.DateTimeField(null=True, blank=True)),
                ('dropoff_time', models.DateTimeField(null=True, blank=True)),
                ('passengers', models.IntegerField(null=True, blank=True)),
                ('trip_time', models.IntegerField(null=True, blank=True)),
                ('trip_distance', models.DecimalField(null=True, max_digits=5, decimal_places=2, blank=True)),
                ('pickup_longitude', models.DecimalField(null=True, max_digits=10, decimal_places=7, blank=True)),
                ('pickup_latitude', models.DecimalField(null=True, max_digits=10, decimal_places=7, blank=True)),
                ('dropoff_longitude', models.DecimalField(null=True, max_digits=10, decimal_places=7, blank=True)),
                ('dropoff_latitude', models.DecimalField(null=True, max_digits=10, decimal_places=7, blank=True)),
            ],
            options={
                'db_table': 'Taxi_trip_data',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='ConsumerComplaint',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_received', models.DateField(null=True, verbose_name='Date received', blank=True)),
                ('product', models.CharField(max_length=255, null=True, verbose_name='Product', blank=True)),
                ('sub_product', models.CharField(max_length=255, null=True, verbose_name='Sub-product', blank=True)),
                ('issues', models.TextField(null=True, verbose_name='Issue', blank=True)),
                ('sub_issue', models.TextField(null=True, verbose_name='Sub-issue', blank=True)),
                ('consumer_complaint_narrative', models.TextField(null=True, verbose_name='Consumer complaint narrative', blank=True)),
                ('company_public_response', models.TextField(null=True, verbose_name='Company public response', blank=True)),
                ('company', models.CharField(max_length=255, null=True, verbose_name='Company', blank=True)),
                ('state', models.CharField(max_length=10, null=True, verbose_name='State', blank=True)),
                ('zip_code', models.CharField(max_length=20, null=True, verbose_name='ZIP code', blank=True)),
                ('tags', models.TextField(null=True, verbose_name='Tags', blank=True)),
                ('consumer_complaint_provided', models.CharField(max_length=128, null=True, verbose_name='Consumer consent provided?', blank=True)),
                ('submitted_via', models.CharField(max_length=50, null=True, verbose_name='Submitted via', blank=True)),
                ('date_sent_to_company', models.DateField(null=True, verbose_name='Date sent to company', blank=True)),
                ('company_response', models.CharField(max_length=255, null=True, verbose_name='Company response to consumer', blank=True)),
                ('timely_response', models.CharField(max_length=50, null=True, verbose_name='Timely response?', blank=True)),
                ('consumer_disputed', models.CharField(max_length=50, null=True, verbose_name='Consumer disputed?', blank=True)),
                ('complaint_id', models.CharField(max_length=50, null=True, verbose_name='Complaint ID', blank=True)),
            ],
            options={
                'db_table': 'consumer_complaints',
            },
        ),
    ]
