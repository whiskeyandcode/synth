from django.db import models


class Trip(models.Model):
    medallion = models.CharField(max_length=50, blank=True)
    hack_license = models.CharField(max_length=50, blank=True)
    vendor_id = models.CharField(max_length=50, blank=True)
    rate_code = models.IntegerField(blank=True, null=True)
    store_fwd_flag = models.CharField(max_length=10, blank=True)
    pickup_time = models.DateTimeField(blank=True, null=True)
    dropoff_time = models.DateTimeField(blank=True, null=True)
    passengers = models.IntegerField(blank=True, null=True)
    trip_time = models.IntegerField(blank=True, null=True)
    trip_distance = models.DecimalField(max_digits=5, decimal_places=2,
                                        blank=True, null=True)
    pickup_longitude = models.DecimalField(max_digits=10, decimal_places=7,
                                           blank=True, null=True)
    pickup_latitude = models.DecimalField(max_digits=10, decimal_places=7,
                                          blank=True, null=True)
    dropoff_longitude = models.DecimalField(max_digits=10, decimal_places=7,
                                            blank=True, null=True)
    dropoff_latitude = models.DecimalField(max_digits=10, decimal_places=7,
                                           blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Taxi_trip_data'
