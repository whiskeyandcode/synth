from django.db import models
from django import forms
from django.utils.translation import ugettext_lazy as _


class ConsumerComplaint(models.Model):
    date_received = models.DateField(verbose_name=_("Date received"), blank=True, null=True)
    product = models.CharField(max_length=255, verbose_name=_("Product"), blank=True, null=True)
    sub_product = models.CharField(max_length=255, verbose_name=_("Sub-product"), blank=True, null=True)
    issues = models.TextField(verbose_name=_("Issue"), blank=True, null=True)
    sub_issue = models.TextField(verbose_name=_("Sub-issue"), blank=True, null=True)
    consumer_complaint_narrative = models.TextField(verbose_name=_("Consumer complaint narrative"), blank=True, null=True)
    company_public_response = models.TextField(verbose_name=_("Company public response"), blank=True, null=True)
    company = models.CharField(max_length=255, verbose_name=_("Company"), blank=True, null=True)
    state = models.CharField(max_length=10, verbose_name=_("State"), blank=True, null=True)
    zip_code = models.CharField(max_length=20, verbose_name=_("ZIP code"), blank=True, null=True)
    tags = models.TextField(verbose_name=_("Tags"), blank=True, null=True)
    consumer_complaint_provided = models.CharField(max_length=128, verbose_name=_("Consumer consent provided?"), blank=True, null=True)
    submitted_via = models.CharField(max_length=50, verbose_name=_("Submitted via"), blank=True, null=True)
    date_sent_to_company = models.DateField(verbose_name=_("Date sent to company"), blank=True, null=True)
    company_response = models.CharField(max_length=255, verbose_name=_("Company response to consumer"), blank=True, null=True)
    timely_response = models.CharField(max_length=50, verbose_name=_("Timely response?"), blank=True, null=True)
    consumer_disputed = models.CharField(max_length=50, verbose_name=_("Consumer disputed?"), blank=True, null=True)
    complaint_id = models.CharField(max_length=50, verbose_name=_("Complaint ID"), blank=True, null=True)

    @classmethod
    def load_data(cls, filename):
        import csv
        f = open(filename)
        reader = csv.reader(f)
        # skip the header
        reader.next()
        for row in reader:
            (date_received, product, sub_product, issues, sub_issue,
             consumer_complaint_narrative, company_public_response, company,
             state, zip_code, tags, consumer_complaint_provided, submitted_via,
             date_sent_to_company, company_response, timely_response,
             consumer_disputed, complaint_id) = row
            form = ConsumerComplaintForm({
                'date_received': date_received,
                'product': product,
                'sub_product': sub_product,
                'issues': issues,
                'sub_issue': sub_issue,
                'consumer_complaint_narrative': consumer_complaint_narrative,
                'company_public_response': company_public_response,
                'company': company,
                'state': state,
                'zip_code': zip_code,
                'tags': tags,
                'consumer_complaint_provided': consumer_complaint_provided,
                'submitted_via': submitted_via,
                'date_sent_to_company': date_sent_to_company,
                'company_response': company_response,
                'timely_response': timely_response,
                'consumer_disputed': consumer_disputed,
                'complaint_id': complaint_id})
            if form.is_valid():
                cc = cls(**form.cleaned_data)
                cc.save()

    class Meta:
        db_table = 'consumer_complaints'




class ConsumerComplaintForm(forms.ModelForm):
    class Meta:
        model = ConsumerComplaint
        fields = '__all__'
