from django.db import models


class NycInspections(models.Model):
    camis = models.IntegerField(db_column='CAMIS', blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=50, blank=True, null=True)  # Field name made lowercase.
    borough = models.CharField(db_column='Borough', max_length=20, blank=True, null=True)  # Field name made lowercase.
    building = models.IntegerField(db_column='Building', blank=True, null=True)  # Field name made lowercase.
    street = models.CharField(db_column='Street', max_length=50, blank=True, null=True)  # Field name made lowercase.
    zipcode = models.CharField(db_column='Zipcode', max_length=10, blank=True, null=True)  # Field name made lowercase.
    phone = models.CharField(db_column='Phone', max_length=12, blank=True, null=True)  # Field name made lowercase.
    cuisine = models.CharField(db_column='Cuisine', max_length=30, blank=True, null=True)  # Field name made lowercase.
    inspection_date = models.DateField(db_column='Inspection_date', blank=True, null=True)  # Field name made lowercase.
    action = models.CharField(db_column='Action', max_length=50, blank=True, null=True)  # Field name made lowercase.
    violation_code = models.CharField(db_column='Violation_code', max_length=3, blank=True, null=True)  # Field name made lowercase.
    violation_description = models.CharField(db_column='Violation_description', max_length=240, blank=True, null=True)  # Field name made lowercase.
    critical_flag = models.CharField(db_column='Critical_flag', max_length=30, blank=True, null=True)  # Field name made lowercase.
    score = models.IntegerField(db_column='Score', blank=True, null=True)  # Field name made lowercase.
    grade = models.CharField(db_column='Grade', max_length=1, blank=True, null=True)  # Field name made lowercase.
    grade_date = models.DateField(db_column='Grade_date', blank=True, null=True)  # Field name made lowercase.
    record_date = models.DateField(db_column='Record_date', blank=True, null=True)  # Field name made lowercase.
    inspection_type = models.CharField(db_column='Inspection_type', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NYC_inspections'
