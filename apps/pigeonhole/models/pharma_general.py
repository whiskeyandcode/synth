def full_name(row):
    return '{0}{1}{2}'.format(
        '%s ' % row['Physician_First_Name'] if row['Physician_First_Name'] else '',
        '%s ' % row['Physician_Middle_Name'] if row['Physician_Middle_Name'] else '',
        '%s' % row['Physician_Last_Name'] if row['Physician_Last_Name'] else '')

filename = '/Users/ppadmanabh/Projects/data/csv/openpayments/2013/general.csv'
dtypes = {
    'Contextual_Information': str,
    'Name_of_Associated_Covered_Drug_or_Biological1': str,
    'Name_of_Associated_Covered_Drug_or_Biological2': str,
    'Name_of_Associated_Covered_Drug_or_Biological3': str,
    'Name_of_Associated_Covered_Drug_or_Biological4': str,
    'Name_of_Associated_Covered_Drug_or_Biological5': str,
    'Name_of_Associated_Covered_Device_or_Medical_Supply1': str,
    'Name_of_Associated_Covered_Device_or_Medical_Supply2': str,
    'Name_of_Associated_Covered_Device_or_Medical_Supply3': str,
    'Name_of_Associated_Covered_Device_or_Medical_Supply4': str,
    'Name_of_Associated_Covered_Device_or_Medical_Supply5': str,
    'Name_of_Third_Party_Entity_Receiving_Payment_or_Transfer_of_Value': str,
    'NDC_of_Associated_Covered_Drug_or_Biological1': str,
    'NDC_of_Associated_Covered_Drug_or_Biological3': str,
    'NDC_of_Associated_Covered_Drug_or_Biological2': str,
    'NDC_of_Associated_Covered_Drug_or_Biological5': str,
    'NDC_of_Associated_Covered_Drug_or_Biological4': str,
    'Physician_Name_Suffix': str,
    'Physician_First_Name': str,
    'Physician_Last_Name': str,
    'Physician_Middle_Name': str,
    'Physician_License_State_code3': str,
    'Physician_License_State_code2': str,
    'Recipient_Primary_Business_Street_Address_Line1': str,
    'Recipient_Primary_Business_Street_Address_Line2': str,
    'Recipient_Zip_Code': str,
    'Recipient_City': str,
    'Recipient_State': str,
    'Recipient_Country': str,
    'Recipient_Province': str,
    'Recipient_Postal_Code': str,
    'Physician_License_State_code4': str,
    'Physician_License_State_code5': str,
    'Teaching_Hospital_Name': str,
    'Third_Party_Equals_Covered_Recipient_Indicator': str
}

reader = pd.read_csv(
    filename, chunksize=100000,
    dtype=dtypes, parse_dates=True,
    na_values=[''], index_col='Record_ID')

    for idx, df in enumerate(reader):
        df = df.fillna('')
        df['Physician_Name'] = df.apply(lambda row: full_name(row), axis=1)
        df.to_csv('/Users/ppadmanabh/Projects/data/csv/openpayments/2013/subfiles/g%s.csv' % idx)
        print 'Wrote %s records to g%s.csv' % (len(df), idx)
