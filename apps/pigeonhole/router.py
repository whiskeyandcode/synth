class PigeonHoleRouter(object):
    """
    A router to control all database operations on models in the
    pigeonhole app.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read pigeonhole models go to 'synth'.
        """
        if model._meta.app_label == 'pigeonhole':
            return 'synth'
        return 'default'

    def db_for_write(self, model, **hints):
        """
        Attempts to write pigeonhole models go to 'synth'.
        """
        if model._meta.app_label == 'pigeonhole':
            return 'synth'

        return 'default'

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the pigeonhole app is involved.
        """
        if obj1._meta.app_label == 'pigeonhole' or \
                obj2._meta.app_label == 'pigeonhole':
            return True
        return 'default'

    def allow_migrate(self, db, app_label, model=None, **hints):
        """
        Make sure the pigeonhole app only appears in the 'synth'
        database.
        """
        if app_label == 'pigeonhole':
            return db == 'synth'

        return None
