from functools import partial
from elasticsearch import RequestsHttpConnection
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Q, A
from elasticsearch_dsl import Search as _search

ES_HOSTS = {
    'uat': {
        'hosts': 'web.uat.dragonglass.me:9200',
        'connection_class': RequestsHttpConnection,
        'http_auth': "dguser:4Welcome"
    },
    'int': {
        'hosts': 'web.int.dragonglass.me:9200',
        'connection_class': RequestsHttpConnection,
        'http_auth': "dguser:4Welcome"
    }
}
connections.configure(**ES_HOSTS)



ENV = 'uat'
Search = partial(_search, using=ENV, index='openpayments-v1-default')


# Data columns (total 62 columns):
# Covered_Recipient_Type                                               100000 non-null object
# Teaching_Hospital_ID                                                 130 non-null float64
# Teaching_Hospital_Name                                               130 non-null object
# Physician_Profile_ID                                                 99870 non-null float64
# Physician_First_Name                                                 99869 non-null object
# Physician_Middle_Name                                                87268 non-null object
# Physician_Last_Name                                                  99868 non-null object
# Physician_Name_Suffix                                                6044 non-null object
# Recipient_Primary_Business_Street_Address_Line1                      100000 non-null object
# Recipient_Primary_Business_Street_Address_Line2                      39673 non-null object
# Recipient_City                                                       100000 non-null object
# Recipient_State                                                      100000 non-null object
# Recipient_Zip_Code                                                   100000 non-null object
# Recipient_Country                                                    100000 non-null object
# Recipient_Province                                                   0 non-null object
# Recipient_Postal_Code                                                0 non-null object
# Physician_Primary_Type                                               99870 non-null object
# Physician_Specialty                                                  99870 non-null object
# Physician_License_State_code1                                        99870 non-null object
# Physician_License_State_code2                                        577 non-null object
# Physician_License_State_code3                                        52 non-null object
# Physician_License_State_code4                                        0 non-null object
# Physician_License_State_code5                                        0 non-null object
# Submitting_Applicable_Manufacturer_or_Applicable_GPO_Name            100000 non-null object
# Applicable_Manufacturer_or_Applicable_GPO_Making_Payment_ID          100000 non-null int64
# Applicable_Manufacturer_or_Applicable_GPO_Making_Payment_Name        100000 non-null object
# Applicable_Manufacturer_or_Applicable_GPO_Making_Payment_State       100000 non-null object
# Applicable_Manufacturer_or_Applicable_GPO_Making_Payment_Country     100000 non-null object
# Total_Amount_of_Payment_USDollars                                    100000 non-null float64
# Date_of_Payment                                                      100000 non-null object
# Number_of_Payments_Included_in_Total_Amount                          100000 non-null int64
# Form_of_Payment_or_Transfer_of_Value                                 100000 non-null object
# Nature_of_Payment_or_Transfer_of_Value                               100000 non-null object
# City_of_Travel                                                       2817 non-null object
# State_of_Travel                                                      2732 non-null object
# Country_of_Travel                                                    2817 non-null object
# Physician_Ownership_Indicator                                        99870 non-null object
# Third_Party_Payment_Recipient_Indicator                              100000 non-null object
# Name_of_Third_Party_Entity_Receiving_Payment_or_Transfer_of_Value    1498 non-null object
# Charity_Indicator                                                    94066 non-null object
# Third_Party_Equals_Covered_Recipient_Indicator                       1498 non-null object
# Contextual_Information                                               2049 non-null object
# Delay_in_Publication_Indicator                                       100000 non-null object
# Dispute_Status_for_Publication                                       100000 non-null object
# Product_Indicator                                                    100000 non-null object
# Name_of_Associated_Covered_Drug_or_Biological1                       93369 non-null object
# Name_of_Associated_Covered_Drug_or_Biological2                       29424 non-null object
# Name_of_Associated_Covered_Drug_or_Biological3                       16 non-null object
# Name_of_Associated_Covered_Drug_or_Biological4                       7 non-null object
# Name_of_Associated_Covered_Drug_or_Biological5                       2 non-null object
# NDC_of_Associated_Covered_Drug_or_Biological1                        93303 non-null object
# NDC_of_Associated_Covered_Drug_or_Biological2                        29420 non-null object
# NDC_of_Associated_Covered_Drug_or_Biological3                        12 non-null object
# NDC_of_Associated_Covered_Drug_or_Biological4                        6 non-null object
# NDC_of_Associated_Covered_Drug_or_Biological5                        2 non-null object
# Name_of_Associated_Covered_Device_or_Medical_Supply1                 4517 non-null object
# Name_of_Associated_Covered_Device_or_Medical_Supply2                 634 non-null object
# Name_of_Associated_Covered_Device_or_Medical_Supply3                 30 non-null object
# Name_of_Associated_Covered_Device_or_Medical_Supply4                 6 non-null object
# Name_of_Associated_Covered_Device_or_Medical_Supply5                 0 non-null object
# Program_Year                                                         100000 non-null int64
# Payment_Publication_Date                                             100000 non-null object
